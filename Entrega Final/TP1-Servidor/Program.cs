﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Server
{
    public class Domicilio
    {
        public string Endereco { get; set; }
        public string Municipio { get; set; }
        public string Operador { get; set; }
    }

    public class ResultadoProcessamento
    {
        public string Municipio { get; set; }
        public int NumeroDomicilios { get; set; }
        public int Sobreposicoes { get; set; }
    }

    


    public class ProcessadorCobertura
    {

        public void OrganizarCSVporMunicipio(string filePath)
        {
            // Ler o arquivo CSV
            var lines = File.ReadAllLines(filePath);

            // Separar o cabeçalho e os dados
            var header = lines[0];
            var data = lines.Skip(1);

            // Organizar os dados por município
            var dataOrganizada = data
                .Select(line => new
                {
                    Line = line,
                    Municipio = line.Split(';').Length > 1 ? line.Split(';')[1].Trim() : ""
                })
                .OrderBy(item => item.Municipio)
                .Select(item => item.Line);

            // Juntar o cabeçalho e os dados organizados
            var linesOrganizadas = new[] { header }.Concat(dataOrganizada);

            // Salvar o arquivo CSV organizado
            File.WriteAllLines(filePath, linesOrganizadas);
        }



        public List<Domicilio> ProcessarCSV(string _filepath)
        {
            var domicilios = new List<Domicilio>();
            using (var reader = new StreamReader(_filepath))
            {
                reader.ReadLine(); // Pular cabeçalho
                while (!reader.EndOfStream)
                {
                    var linha = reader.ReadLine();
                    var valores = linha.Split(';');

                    // Verificar se a linha possui o número correto de colunas (3 colunas: Endereco, Municipio, Operador)
                    if (valores.Length != 3)
                    {
                        continue; // Ignorar esta linha e continuar com a próxima
                    }

                    var domicilio = new Domicilio
                    {
                        Endereco = valores[0].Trim(),
                        Municipio = valores[1].Trim(),
                        Operador = valores[2].Trim()
                    };

                    // Verificar se o domicílio já foi adicionado à lista
                    if (!domicilios.Any(d => d.Endereco == domicilio.Endereco && d.Municipio == domicilio.Municipio && d.Operador == domicilio.Operador))
                    {
                        domicilios.Add(domicilio);
                    }
                }
            }

            return domicilios;
        }

        public List<ResultadoProcessamento> Processar(List<Domicilio> domicilios)
        {
            var resultado = domicilios.GroupBy(d => d.Municipio)
                .Select(g => new ResultadoProcessamento
                {
                    Municipio = g.Key,
                    NumeroDomicilios = g.Count(),
                    Sobreposicoes = g.GroupBy(d => d.Endereco).Count(gr => gr.Count() > 1)
                }).ToList();

            return resultado;
        }
    }

    class Program
    {
        static Mutex mutex = new Mutex();

        static void Main(string[] args)
        {
            TcpListener listener = new TcpListener(IPAddress.Any, 8888);
            listener.Start();

            Console.WriteLine("Server started");

            while (true)
            {
                TcpClient client = listener.AcceptTcpClient();

                Thread thread = new Thread(() => HandleClient(client));
                thread.Start();
            }
        }

        static void HandleClient(TcpClient client)
        {
            Console.WriteLine("Client connected...");
            NetworkStream stream = client.GetStream();

            SendResponse(stream, "100 OK");

            byte[] buffer = new byte[1024];
            int bytesRead;
            string data = "";

            while ((bytesRead = stream.Read(buffer, 0, buffer.Length)) > 0)
            {
                data += Encoding.ASCII.GetString(buffer, 0, bytesRead);

                if (data.IndexOf("QUIT") > -1)
                {
                    SendResponse(stream, "400 BYE");
                    break;
                }

                string response = ProcessMessage(stream, data);

                SendResponse(stream, response);

                // Clear data buffer
                data = "";
            }

            client.Close();
        }

        static string ProcessMessage(NetworkStream stream, string message)
        {
            if (message.StartsWith("SEND"))
            {
                mutex.WaitOne();
                try
                {
                    // Encontre a posição do primeiro caractere após "SEND| "
                    int indexOfFileName = message.IndexOf("SEND| ") + "SEND| ".Length;
                    // Encontre a posição do primeiro caractere após o nome do arquivo
                    int indexOfData = message.IndexOf(Environment.NewLine, indexOfFileName) + Environment.NewLine.Length;

                    // Obtenha o nome do arquivo e o conteúdo do arquivo separadamente
                    string fileName = message.Substring(indexOfFileName, indexOfData - indexOfFileName - Environment.NewLine.Length).Trim();
                    string fileContent = message.Substring(indexOfData);

                    string[] fileLines = fileContent.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
                    StringBuilder contentWithoutHeader = new StringBuilder();
                    for (int i = 1; i < fileLines.Length; i++)
                    {
                        contentWithoutHeader.AppendLine(fileLines[i]);
                    }

                    Console.WriteLine("Receiving file: " + fileName);

                    string savePath = Path.Combine(Directory.GetCurrentDirectory(), "combined_data.csv");
                    File.AppendAllText(savePath, contentWithoutHeader.ToString());
                    Console.WriteLine($"File '{fileName}' received and saved at '{savePath}'.");

                    var processadorCobertura = new ProcessadorCobertura();
                    processadorCobertura.OrganizarCSVporMunicipio(savePath);
                    var domicilios = processadorCobertura.ProcessarCSV(savePath);
                    var resultado = processadorCobertura.Processar(domicilios);

                    StringBuilder responseBuilder = new StringBuilder();
                    responseBuilder.AppendLine("Resultados:");

                    string Fresponse = "\n";
                    Console.WriteLine("\n\n");
                    foreach (var item in resultado)
                    {
                        Console.WriteLine($"Município: {item.Municipio}, Número de Domicílios: {item.NumeroDomicilios}, Sobreposições: {item.Sobreposicoes}");
                        string resposta = ($"Município: {item.Municipio}, Número de Domicílios: {item.NumeroDomicilios}, Sobreposições: {item.Sobreposicoes}");
                        Fresponse = Fresponse + "\n" + resposta;
                    }

                    Fresponse = Fresponse + "\n\n" + "Server Response: 200 OK" + "\n\n";

                    SendResponse(stream, Fresponse);
                }
                finally { mutex.ReleaseMutex(); }
                return "";
            }
            else
            {
                return "400 Bad Request";
            }

            return "";
        }



        static void SendResponse(NetworkStream stream, string message)
        {
            byte[] buffer = Encoding.UTF8.GetBytes(message);
            stream.Write(buffer, 0, buffer.Length);
        }
    }

}