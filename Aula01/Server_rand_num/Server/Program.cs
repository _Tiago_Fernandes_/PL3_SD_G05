﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;

class TCPServer
{
    static void Main(string[] args)
    {
        try
        {
            // Set the IP address and port number to listen on
            IPAddress localIP = IPAddress.Parse("25.1.54.139"); // listen on all available network interfaces
            int port = 8888;

            // Create a TCP listener socket
            TcpListener listener = new TcpListener(localIP, port);

            // Start listening for incoming connections
            listener.Start();
            Console.WriteLine("Server listening on port " + port);

            while (true)
            {
                // Accept incoming client connections
                TcpClient client = listener.AcceptTcpClient();
                Console.WriteLine("Client connected: " + client.Client.RemoteEndPoint.ToString());

                // Create a network stream to send and receive data
                NetworkStream stream = client.GetStream();

                // Receive data from the client
                byte[] data = new byte[1024];
                int bytes = stream.Read(data, 0, data.Length);
                string message = Encoding.ASCII.GetString(data, 0, bytes);
                Console.WriteLine("Received message: " + message);

                // Send a response back to the client
                Random random = new Random();
                int randomNumber = random.Next(0, 100);
                string response = randomNumber.ToString(); ;
                data = Encoding.ASCII.GetBytes(response);
                stream.Write(data, 0, data.Length);
                Console.WriteLine(response);

                // Close the client socket and stream
                stream.Close();
                client.Close();
            }
        }
        catch (Exception e)
        {
            Console.WriteLine("Exception: " + e.Message);
        }
    }
}